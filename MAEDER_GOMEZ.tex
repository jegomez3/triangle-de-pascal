\usepackage{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{xcolor}

\title{"Le Triangle de Pascal"}
\author{MAEDER Camille, GOMEZ Jeanne}
\date{11/01/2024}

\begin{document}

\maketitle
\tableofcontents
\section{Introduction}
Lorsque nous évoquons Blaise Pascal, nous ne pensons pas premièrement à son triangle. Pourtant, ce dernier est largement utilisé dans les mathématiques. En effet, nous pouvons le retrouver dans plusieurs théorèmes d'autres scientifiques tels que Bernouilli Newton du fait de son utilité et de sa simplicité d'utilisation. Dans un premier temps nous introduirons la factorielle est les coefficients binomiaux afin de pouvoir comprendre comment est construit le triangle de Pascal, puis nous en verrons l'application dans deux théorèmes différents. 
\section{Factorielle et coefficients binomiaux}
\subsection{Factorielle}
Etablissons que l'ensemble $\mathbb{N}$ correspond à l'ensemble des entiers naturels, donc les entiers définis entre $[0, +\infty[$.\\
L'opération factorielle est une opération qui se note avec un point d'exclamation précédé par un nombre entier $n$ compris dans $\mathbb{N}$ : $ n! $. Cette notation se lit "Factorielle de $n$" et exprime le fait de mutiplier $n$ par tous les nombres entiers $k$ tels que $ 1 \le k \le n$. La définition formelle de la factorielle de $n$ est:
\begin{equation*}
n! = \prod_{1 \le k \le n} i = 1 \times 2 \times ... \times (n - 1) \times n
\end{equation*} 
\\
Voyons quelques exemples: 

$3! = 1 \times 2 \times 3 = 6$

$6! = 1 \times 2 \times 3 \times 4 \times 5 \times 6 = 720$

$11! = 1 \times 2 \times 3 \times 4 \times 5 \times 6 \times 7 \times 8 \times 9 \times 10 \times 11 = 39916800$

$0! = 1! = 1$  (Etant donné que l'élément neutre de la multiplication est 1)
\\\\
La fonction qui a $n$ associe $n!$ est donc strictement croissante sur $\mathbb{R}$ et définie sur $[1, +\infty[$:
\\\\
\includegraphics[scale=0.5]{Fact 20.png}
\\\\
La factorielle d'un nombre est fréquemment utilisée pour définir le nombre de formations différentes que l'on peut réaliser à partir de $n$: pour 22 élèves, combien de formations différentes de la classe pouvons nous avoir ? La réponse est $22!$
\subsection{Coefficients binomiaux}

Le coefficient binomial, noté \(\binom{n}{k}\) avec $k$ \(\leq\) $n$, est comme dit précédemment ce qui compose le triangle de pascal. Il est beaucoup utilisé en statistiques et en dénombrement, parce qu'il exprime le nombre d'ensembles différents à $k$ éléments que l'on peut créer avec un ensemble de départ contenant $n$ éléments.\\
On peut donc obtenir le résultat de \(\binom{n}{k}\), soit en utilisant le triangle de Pascal, soit en le calculant avec la formule suivante :
\begin{equation*}
\binom{n}{k} = \frac{n!}{k! \times (n-k)!}
\end{equation*}
\\\\
\paragraph{}
\(\binom{4}{2}\) = \(\frac{4!}{2! \times (4-2)!}\) = \(\frac{4 \times 3 \times 2 \times 1}{2 \times 1 \; \times \; 2 \times 1}\) = \(\frac{24}{4}\) = 6\\

\begin{tabular}{ c c c c c c c }
       & 0  & 1  & \color{orange} 2 \color{black} & 3  & 4  & 5  \\
     0  & 1 &   &   &  &  &   \\
     1  & 1  & 1  &  &  &  &   \\
     2  & 1  & 2 & 1 &  &  &   \\
     3  & 1 & 3 & 3 & 1 &  &   \\
     \color{orange} 4 \color{black}  & 1 & 4 & \color{red} 6 \color{black} & 4 & 1 &  \\
     5  & 1 & 5 & 10 & 10 & 5 & 1  \\

\end{tabular}
\\
\paragraph{}

\(\binom{5}{3}\) = \(\frac{5!}{3! \times (5-3)!}\) = \(\frac{5 \times 4 \times 3 \times 2 \times 1}{3 \times 2 \times 1 \; \times \; 2 \times 1}\) = \(\frac{120}{12}\) = 10\\

\begin{tabular}{ c c c c c c c  }
       & 0  & 1  & 2 & \color{orange} 3 \color{black} & 4  & 5  \\
     0  & 1 &   &   &  &  &   \\
     1  & 1  & 1  &  &  &  &   \\
     2  & 1  & 2 & 1 &  &  &   \\
     3  & 1 & 3 & 3 & 1 &  &   \\
     4  & 1 & 4 & 6 & 4 & 1 &  \\
     \color{orange} 5 \color{black} & 1 & 5 & 10 & \color{red} 10 \color{black} & 5 & 1  \\

\end{tabular}
\\\\

On peut utiliser les coefficients binomiaux pour déterminer le nombre de différents groupes de quatre personnes que l'on peut faire dans une classe de 24 élèves,  \(\binom{24}{4}\) = 10626 . On peut donc former 10626 différents groupes de 4 dans une classe de 24 (à noter que pour utiliser les coefficients binomiaux, il faut que l'ordre ne soit pas important, c'est-à-dire le groupe (A,B) est le même que le groupe (B,A)).


\section{Théorèmes}
\subsection{Relation de Pascal}

La relation de Pascal est le calcul que nous faisons "intuitivement" quand nous écrivons le triangle de Pascal. En effet, pour calculer une valeur du triangle de Pascal, nous additionnons la valeur au dessus de celle souhaitée avec la valeur à la gauche de la valeur au dessus de celle souhaitée, ce qui revient à utiliser cette formule : \\
\begin{equation*}
\binom{n}{k} = \binom{n-1}{k-1} + \binom{n-1}{k} 
\end{equation*}
\\
Démontrons-la par le calcul :\\\\
\begin{equation*}
\begin{split} 
\binom{n-1}{k-1} + \binom{n-1}{k} = \frac{(n-1)!}{(k-1)! \; \times \; (n-1-(k-1))!} +  \frac{(n-1)!}{k! \; \times \; (n-1-k)!} \\ = \frac{(n-1)!}{(k-1)! \; \times \; (n-k)!} + \frac{(n-1)!}{k! \; \times \; (n-1-k)!}  = \frac{(n-1)! \times k}{k! \; \times \; (n-k)!} +\frac{(n-1)! \times (n-k)}{k! \; \times \; (n-k)!} \\ = \frac{(n-1)! \times (k+n-k)}{k! \; \times \; (n-k)!} = \frac{(n-1)! \times n!}{k! \; \times \; (n-k)!} = \frac{n!}{k! \; \times \; (n-k)!} = \binom{n}{k}
\end{split} 
\end{equation*}
\\
\paragraph{}
\(\binom{4-1}{1-1} + \binom{4-1}{1}  =\binom{3}{0} + \binom{3}{1}  = \frac{3!}{0! \times (3-0)!} + \frac{3!}{1! \times (3-1)!} = \frac{3 \times 2 \times 1}{3 \times 2 \times 1}  + \frac{3 \times 2 \times 1}{1 \times 2 \times 1}  = \frac{6}{6} + \frac{6}{2} = 4 = \binom{4}{1}\)
\\\\
\begin{tabular}{ c c c c c c c  }
       & 0  & 1  & 2 &  3  & 4  & 5  \\
     0  & 1 &   &   &  &  &   \\
     1  & 1  & 1  &  &  &  &   \\
     2  & 1  & 2 & 1 &  &  &   \\
     3  & \color{blue} 1 \color{black} & \color{orange} 3 \color{black} & 3 & 1 &  &   \\
     4  & 1 & \color{red} 4 \color{black} & 6 & 4 & 1 &  \\
     5  & 1 & 5 & 10 & 10 & 5 & 1  \\

\end{tabular} \\\\
\( \color{blue} 1 \color{black} + \color{orange} 3 \color{black} = \color{red} 4 \color{black} \)
\\\\
\paragraph{}
\(\binom{3-1}{2-1} + \binom{3-1}{2}  =\binom{2}{1} + \binom{2}{2}  = \frac{2!}{1! \times (2-1)!} + \frac{2!}{2! \times (2-2)!} = \frac{2 \times 1}{1 \times 1}  + \frac{2 \times 1}{2 \times 1 \times 1}  = \frac{2}{1} + \frac{2}{2} = 2 + 1 = 3 = \binom{3}{2}\)
\\\\
\begin{tabular}{ c c c c c c c  }
       & 0  & 1  & 2 &  3  & 4  & 5  \\
     0  & 1 &   &   &  &  &   \\
     1  & 1  & 1  &  &  &  &   \\
     2  & 1  & \color{blue} 2 \color{black} & \color{orange} 1 \color{black} &  &  &   \\
     3  & 1 & 3 & \color{red} 3 \color{black} & 1 &  &   \\
     4  & 1 & 4 & 6 & 4 & 1 &  \\
     5  & 1 & 5 & 10 & 10 & 5 & 1  \\

\end{tabular} \\\\
\( \color{blue} 2 \color{black} + \color{orange} 1 \color{black} = \color{red} 3 \color{black} \)\\

\subsection{Formule du binôme de Newton}

La formule du binôme de Newton est utilisée pour trouver le développement d'une puissance entière d'un binôme $x$ et $y$ (réels ou complexes). Elle est définie ainsi: 
\begin{equation*}
(x + y)^n = \sum_{k = 0}^{n} \binom{n}{k} x^ky^{n-k}
\end{equation*}
ou ainsi:
\begin{equation*}
(x - y)^n = \sum_{k = 0}^{n} \binom{n}{k} x^k(-y)^{n-k}
\end{equation*}
\\\\
Faisons-en la preuve par récurrence:
\begin{itemize}
\item \underline{Initialisation:}

Toute expression à la puissance 0 vaut 1.
pour $n = 0$, $(x + y)^0 = \sum\limits_{k = 0}^{0} \binom{0}{0} x^0y^{0-0} = 1 \times x^0 \times y^0 = 1 \times 1 \times 1 = 1$.

La formule fonctionne au rang $n = 0$
\\
\item \underline{Hérédité:}

Toute expression à la puissance 1 est égale à elle-même ($2^1 = 2$, $3xy^1 = 3xy$).

pour $n = 1$, $(x + y)^1 = \sum\limits_{k = 0}^{1} \binom{1}{0} x^0y^{1-0} + \sum\limits_{k = 1}^{1} \binom{1}{1} x^1y^{1-1} = 1 \times x^0 \times y^1 + 1 \times x^1 \times y^0 = 1 \times 1 \times y + 1 \times x \times 1 = y + x$

La formule fonctionne avec un $n > 0$, à savoir au rang $n = 1$.

Au rang $n+1$, on veut obtenir $\sum\limits_{k = 0}^{n + 1} \binom{n + 1}{k} x^ky^{n+1-k}$.

$(x + y)^{n+1} = (x + y)(x + y)^n = (x + y)\sum\limits_{k = 0}^{n} \binom{n}{k} x^ky^{n-k} = \sum\limits_{k = 0}^{n} \binom{n}{k} x^{k+1}y^{n-k} + \sum\limits_{k = 0}^{n} \binom{n}{k} x^ky^{n-(k-1)} = \sum\limits_{k = 1}^{n+1} \binom{n}{k-1} x^ky^{n-(k-1)} + \sum\limits_{k = 0}^{n} \binom{n}{k} x^ky^{n-(k-1)} = \binom{n}{0} x^0y^{n+1} + \sum\limits_{k = 1}^{n} \left(\binom{n}{k-1} + \binom{n}{k}\right)x^ky^{n+1-k} + \binom{n}{n}x^{n+1}y^0 = \binom{n+1}{0}x^0y^{n+1} + \sum\limits_{k = 1}^{n} \binom{n+1}{k}x^ky^{n+1-k} + \binom{n+1}{n+1} x^{n+1}y^0 = \sum\limits_{k = 0}^{n + 1} \binom{n + 1}{k} x^ky^{n+1-k}$.

La formule se vérifie au rang $n+1$
\\
\item \underline{Conclusion:}

Nous avons démontré lors des deux étapes suivantes que la formule était vraie pour tous les entiers $n$, ainsi, pour tout $n$ on a:
\begin{equation*}
(x + y)^n = \sum_{k = 0}^{n} \binom{n}{k} x^ky^{n-k}
\end{equation*} \\\\
\end{itemize}
Si nous développons la formule plusieurs fois avec des puissances différentes, nous finissons par remarquer les formes générales développées du binôme $x$ et $y$ pour chaque puissance:

Pour $n = 2$,   $(x + y)^2 = x^2 + 2xy + y^2$

Pour $n = 3$,   $(x + y)^3 = x^3 + 3x^2y + 3xy^2 + y^3$

Pour $n = 4$,   $(x + y)^4 = x^4 + 4x^3y + 6x^2y^2 + 4xy^3 + y^4$

Pour $n = 5$,   $(x + y)^5 = x^5 + 5x^4y + 10x^3y^2 + 10x^2y^3 + 5xy^4 + y^5$

...
\section{Conclusion}
Au cours de ce document nous avons vu comment calculer le triangle de pascal, ainsi que les équations qui en découlent. Principalement utilisé en statistique et en dénombrement, il est aujourd'hui la base de la pluspart des équations de ces deux matières. On peut notamment le retrouver dans les calculs de loi binomiales les plus simple ou dans les calculs de combinaisons, par exemple.\\
\section{Annexes}
\href{https://fr.wikipedia.org/wiki/Factorielle}{Wikipédia - Factorielle} consulté le 12/01/2024\\
\href{https://fr.wikipedia.org/wiki/Formule_du_binôme_de_Newton}{Wikipédia - Formule du binôme de Newton} consulté le 12/01/2024\\
\href{https://www.maxicours.com/se/cours/coefficients-binomiaux-et-loi-de-pascal/}{coefficients-binomiaux} consulté le 12/01/2024\\
\href{https://fr.wikipedia.org/wiki/Coefficient_binomial}{Wikipédia - Coefficient binomial} consulté le 12/01/2024\\
\href{https://www.techno-science.net/glossaire-definition/Coefficient-binomial.html}{definition coefficient binomial} consulté le 12/01/2024\\
\end{document}
